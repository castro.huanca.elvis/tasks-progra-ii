package simpledrawingboard;

import jdk.swing.interop.SwingInterOpUtils;
import lombok.Data;

@Data
public class Canvas {

    private int width, height;
    private String [][] canvas;
    String [][] vertical;
    String [][] horizontal;
    private static final String ELEMENT_TO_DRAW = "x";

    public Canvas(int width, int height) {
        width += 2;
        height += 2;
        this.height = height ;
        this.width = width ;
        canvas = new String[height][width];
    }

    private void createCanvas() {

        for(int i = 0; i<height; i++){
            for(int j = 0; j<width; j++){
                if(i == 0 || i == height - 1){
                    canvas[i][j]="-";
                }
                else if(j == 0 || j == width -1){
                    canvas[i][j]="|";
                }
                else{
                    canvas[i][j] = " ";
                }
            }
        }
    }

    public String showCanvas (){
        String canvasString = "";
        for(int i = 0; i<height; i++) {
            for (int j = 0; j < width; j++) {
                canvasString += canvas[i][j];
            }
            canvasString += "\n";
        }
        return canvasString;
    }

    public static void main(String[] args) {
        Canvas canvas = new Canvas(5,5);
        canvas.draw(0, 2, 4, 2).draw(2,0,2,4);
    }

    public Canvas draw(int x1, int y1, int x2, int y2) {
        createCanvas();
        vertical = drawHorizontalLines(x1, y1, x2, y2);
        horizontal = drawVerticalLines(x1, y1, x2, y2);
        canvas = mergeCanvas(horizontal, vertical);
        System.out.println(showCanvas());
        return this;
    }

    private String [][] drawHorizontalLines (int x1, int y1, int x2, int y2){
        if(y1 == y2){
            for(int i = x1 + 1; i <= x2 + 1; i++){
                canvas[y1 + 1][i] = "x";
            }
        }
        return canvas;
    }

    private String[][] mergeCanvas (String[][] canvas1, String[][]canvas2){
        String [][] result = canvas;
        for(int i = 0; i<canvas1.length; i++) {
            for (int j = 0; j < canvas1[i].length; j++) {
                if(canvas1[i][j] == " "){
                    if (canvas2[i][j] != " "){
                        result[i][j] = canvas2[i][j];
                    }
                }
            }
        }
        return  result;
    }

    private String[][] drawVerticalLines (int x1, int y1, int x2, int y2){
        if(x1 == x2){
            for(int i = y1 + 1; i <= y2 + 1; i++){
                canvas[i][x1+1] = "x";
            }
        }
        return canvas;
    }


    public Canvas fill(int x, int y, char ch) {
        return this;
    }

    public String drawCanvas() {

        for(int i = 0; i<height; i++) {
            for (int j = 0; j < width; j++) {
                if(horizontal[i][j] == " "){
                    if (vertical[i][j] != " "){
                        canvas[i][j] = vertical[i][j];
                    }
                }
            }
        }
        return "draw the canvas with borders";
    }
}