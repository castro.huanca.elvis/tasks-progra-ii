package findcountofmostfrequentiteminanarray;

import lombok.*;

@Data
public class ArrayAnalyzer {

    private int numberFound;

    private int countRepetitionOfNumber(int itemToCount, int[] numbers){
        int counter = 0;
        for (int iteratorNumber : numbers) {
            if (iteratorNumber == itemToCount) {
                counter++;
            }
        }
        return counter;
    }

    private int updateGreater (int counterGreater, int currentCounter, int currentItem){
        if (currentCounter > counterGreater){
            counterGreater = currentCounter;
            this.numberFound = currentItem;
        }
        return counterGreater;
    }

    public int mostFrequentItemCount(int[] numbers){
        int currentCount = 0;
        int most = 0;
        for (int iteratorNumber : numbers) {
            currentCount = countRepetitionOfNumber(iteratorNumber, numbers);
            most = updateGreater(most, currentCount, iteratorNumber);
        }
        return most;
    }

    public String showNumberWithMostRepetitions(int[] numbers){
        int numberOfRepetitions = mostFrequentItemCount(numbers);
        return "The most frequent number in the array is " + numberFound +
                " and it occurs " + numberOfRepetitions + " times.";
    }

}
