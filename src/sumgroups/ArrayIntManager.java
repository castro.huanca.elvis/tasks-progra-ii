package sumgroups;

import java.util.*;

/**
 * This class corresponds to task 4.
 *
 * @author Elvis Jose Castro Huanca
 */
public class ArrayIntManager {

    /**
     * This method checks if a number is even.
     *
     * @param number for to check.
     * @return true if is even, false if not even.
     */
    private boolean isAnEvenNumber (int number){
        return number % 2 == 0;
    }

    /**
     * This method find consecutive elements of the same type.
     *
     * @param numbers List to Search.
     * @param isEven true if is even, false if not even. For to check with another numbers.
     * @param currentIndex Current index in the list.
     * @return An arraylist with consecutive elements of the same type.
     */
    private ArrayList <Integer> searchConsecutiveNumbersOfTheSameType(int[] numbers, boolean isEven, int currentIndex){

        ArrayList <Integer> consecutiveNumbers = new ArrayList<>();

        for(int i = 0; currentIndex < numbers.length; currentIndex++){
            if (isAnEvenNumber(numbers[currentIndex]) != isEven){
                return consecutiveNumbers;
            }
            else{
                consecutiveNumbers.add(numbers[currentIndex]);
            }
        }
        return consecutiveNumbers;
    }
    
    /**
     * This method adds all elements of a list.
     *
     * @param numbers to add all its elements.
     * @return the total sum.
     */
    private int addArrayListNumbers(ArrayList<Integer> numbers){
        int sum = 0;
        for (int iteratorNumber : numbers) {
            sum += iteratorNumber;
        }
        return sum;
    }

    /**
     * This method joins consecutive elements
     * of the same type at a single index.
     *
     * @param numbers for to join its elements of the same type.
     * @return An updated array with consecutive
     * elements of the same type joined.
     */
    private int[] joinItemsOfTheSameType(int[] numbers){
        ArrayList <Integer> arrayListNumbers = new ArrayList<>();
        ArrayList <Integer> arrayListAux;
        int itemForToAdd;
        boolean currentStatus;
        for (int index = 0; index < numbers.length; index++) {
            currentStatus = isAnEvenNumber(numbers[index]);
            arrayListAux = searchConsecutiveNumbersOfTheSameType(numbers, currentStatus, index);
            itemForToAdd = addArrayListNumbers(arrayListAux);
            arrayListNumbers.add(itemForToAdd);

            index += arrayListAux.size() - 1;
        }
        return changeArrayListToArray(arrayListNumbers);
    }

    /**
     * This method converts an arraylist to an array.
     *
     * @param numbers to convert.
     * @return An array list with all the elements of the array.
     */
    private int[] changeArrayListToArray(ArrayList<Integer> numbers){
        int [] arrayNumbers = new int[numbers.size()];
        for (int i = 0; i < numbers.size(); i++) {
            arrayNumbers[i] = numbers.get(i);
        }
        return arrayNumbers;
    }

    /**
     * This method checks if groups exist in an array.
     *
     * @param numbers to check.
     * @return true if it contains groups,
     * false if it does not contain groups.
     */
    private boolean containsGroups(int[] numbers){
        int length = numbers.length - 1;
        int index = 0;
        boolean status = false;
        while (index < length){
            if (isAnEvenNumber(numbers[index]) == isAnEvenNumber(numbers[index + 1]) ){
                status = true;
                index = length;
            }
            index++;
        }
        return status;
    }

    /**
     * This method adds all the groups and
     * returns the length of an array without groups.
     *
     * @param numbers to sum groups.
     * @return array length.
     */
    public int sumGroups(int[] numbers){
        while (containsGroups(numbers)){
            numbers = joinItemsOfTheSameType(numbers);
        }
        return numbers.length;
    }

}