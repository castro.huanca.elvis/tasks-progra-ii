package mysterycolors;

import java.util.*;

public class MysteryColorAnalyzerImpl implements MysteryColorAnalyzer{
    /**
     * This method will determine how many distinct colors are in the given list
     *
     * <p>
     * Colors are defined via the {@link Color} enum.
     * </p>
     *
     * @param mysteryColors list of colors from which to determine the number of distinct colors
     * @return number of distinct colors
     */
    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        List foundColors = new ArrayList();
        for (Color iteratorColor : mysteryColors) {
            addColor(iteratorColor, foundColors);
        }
        return foundColors.size();
    }

    /**
     * This method checks if o list contains a color.
     *
     * @param color color for the check.
     * @param foundColors list of found colors.
     * @return true if found the color in the list Otherwise returns false.
     */
    private boolean checkIfItContains (Color color, List <Color> foundColors){
        boolean containsTheColor = false;
        if ( foundColors.contains(color) ){
            containsTheColor = true;
        }
        return containsTheColor;
    }

    /**
     * This method add a color to a list.
     *
     *If the color does not exist
     * in the list then it is added.
     *
     * @param color color to add.
     * @param foundColors list where it will be added.
     */
    private void addColor(Color color, List <Color> foundColors) {
        if ( !checkIfItContains(color, foundColors) ){
            foundColors.add(color);
        }
    }



    /**
     * This method will count how often a specific color is in the given list
     *
     * <p>
     * Colors are defined via the {@link Color} enum.
     * </p>
     *
     * @param mysteryColors list of colors from which to determine the count of a specific color
     * @param color         color to count
     * @return number of occurrence of the color in the list.
     */
    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int counter = 0;
        for ( Color iteratorColor : mysteryColors ) {
            if ( compareColors(iteratorColor, color) ){
                counter ++;
            }
        }
        return counter;
    }

    private boolean compareColors (Color color1, Color color2){
        return color1 == color2;
    }
}
