package lombokbuilder;

import lombok.*;

@Builder
@Data
// I changed the name of the class because in java the conventions say
// "use singular names" and People make references to many persons.
public class Person {

    private static final String GREET = "hello";

    private int age;
    private String name, lastName;

    public String greet () {
        return GREET + " my name is " + name;
    }

}
