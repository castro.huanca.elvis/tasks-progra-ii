import findcountofmostfrequentiteminanarray.ArrayAnalyzer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArrayAnalyzerTest {

    ArrayAnalyzer arrayAnalyzer = new ArrayAnalyzer();
    int[] numbers =
            { 1,1,1,
            2,2,2,2,
            3,3,3,3,3,
            4,4,4,4,4,4,
            5,5,5,5,5,
            6,6,6,6,6,6,
            7,7,7,7,7,
            8,8,
            9,9,9,9,9,9,9,9,9,9,
            10,10,10,10,10,10,10,10 };

    @Test
    void showNumberWithMostRepetitionsTest() {
    String messageExpected = "The most frequent number in the array " +
            "is 9 and it occurs 10 times.";

    String messageActual = arrayAnalyzer.showNumberWithMostRepetitions(numbers);

    assertEquals(messageExpected, messageActual);

    }

    @Test
    void countHigherRepetitionTest() {

        int mostRepetitions = arrayAnalyzer.mostFrequentItemCount(numbers);

        assertEquals(10, mostRepetitions);
        assertEquals(5, arrayAnalyzer.mostFrequentItemCount(new int[] {3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3}));
        assertEquals(0, arrayAnalyzer.mostFrequentItemCount(new int[]{}));
    }




}
