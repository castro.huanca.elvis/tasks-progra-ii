import org.junit.Test;
import static org.junit.Assert.assertEquals;

import sumgroups.ArrayIntManager;

public class ArrayIntManagerTest {
    
    ArrayIntManager arrayIntManager = new ArrayIntManager();

    @Test
    public void basicTests() {

        assertEquals(6, arrayIntManager.sumGroups(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9}));
        assertEquals(5, arrayIntManager.sumGroups(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 3, 3, 3, 9, 2}));
        assertEquals(1, arrayIntManager.sumGroups(new int[] {2}));
        assertEquals(2, arrayIntManager.sumGroups(new int[] {1,2}));
        assertEquals(1, arrayIntManager.sumGroups(new int[] {1,1,2,2}));

    }
    
}
