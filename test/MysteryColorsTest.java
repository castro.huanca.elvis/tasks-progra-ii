import mysterycolors.Color;
import mysterycolors.MysteryColorAnalyzerImpl;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MysteryColorsTest {

    MysteryColorAnalyzerImpl mysteryColorAnalyzer = new MysteryColorAnalyzerImpl();
    List <Color> colorsList = new ArrayList<>();

    void addData(){
       colorsList.add(Color.RED);
       colorsList.add(Color.RED);
       colorsList.add(Color.RED);

       colorsList.add(Color.BLUE);
       colorsList.add(Color.BLUE);
       colorsList.add(Color.BLUE);
       colorsList.add(Color.BLUE);
       colorsList.add(Color.BLUE);
       colorsList.add(Color.BLUE);

       colorsList.add(Color.GREEN);
       colorsList.add(Color.GREEN);
       colorsList.add(Color.GREEN);
       colorsList.add(Color.GREEN);

    }

    @Test
    void numberOfDistinctColorsTest() {
        addData();
        int numberOfColors = mysteryColorAnalyzer.numberOfDistinctColors(colorsList);

        assertEquals (3, numberOfColors);

    }

    @Test
    void colorOccurrenceTest() {
        addData();
        int numberOfOccurrencesGreen = mysteryColorAnalyzer.colorOccurrence(colorsList, Color.GREEN);
        int numberOfOccurrencesRed = mysteryColorAnalyzer.colorOccurrence(colorsList, Color.RED);
        int numberOfOccurrencesBlue = mysteryColorAnalyzer.colorOccurrence(colorsList, Color.BLUE);

        assertEquals(4, numberOfOccurrencesGreen);
        assertEquals(3, numberOfOccurrencesRed);
        assertEquals(6, numberOfOccurrencesBlue);

    }
}
