import lombokbuilder.Person;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LombokBuilderTest {

    @Test
    void greetTest() {
        Person person = Person.builder().age(21).name("Juan").lastName("Perez").build();

        String greet = person.greet();

        assertEquals("Juan", person.getName());
        assertEquals( "hello my name is Juan", greet);
        assertEquals(21, person.getAge());
        assertEquals("Perez", person.getLastName());
        
    }
}
